#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

const int N = 10;

void out(int* array, int size) {
    printf("\n");
    for(int i = 0; i < size; i++) {
       for (int j = 0; j < array[i]; ++j) {
           printf("|");
       }
       printf("\n");
    }
    printf("\n");
}

int partition(int* array, int p, int r) {
    out(array, N);
    int less[r - p], more[r - p];
    int key = array[r];
    int left_k = 0, right_k = 0;
    #pragma omp parallel for
    for (int i = p; i < r; i++) {
        if (array[i] < array[r]) {
            less[left_k++] = array[i];
        } else {
            more[right_k++] = array[i];
        }   
    }

    for(int i = 0; i < left_k; i++) {
        array[p + i] = less[i];
    }   

    array[p + left_k] = key;

    for(int i = 0; i < right_k; i++) {
        array[p + left_k + i + 1] = more[i];
    }   
    return p + left_k;
}

void sort(int* a, int p, int r) {
    int div;
    if (p < r) { 
        div = partition(a, p, r); 
        #pragma omp parallel sections
        {   
            #pragma omp section
            sort(a, p, div - 1); 
            #pragma omp section
            sort(a, div + 1, r); 
        }
    }
}

int main() {
    omp_set_num_threads(3);
    srand(time(0));
    int* array = new int[N];
    for(int i = 0; i < N; i++) {
        array[i] = rand() % (N*5) + 1;
    }

    printf("Random:\n");
    for(int i = 0; i < N; i++) {
        printf("%d ", array[i]);
    }
    printf("\n\n");
    sort(array, 0, N - 1);
    printf("Sorted:\n");
    for(int i = 0; i < N; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
    return 0;
}