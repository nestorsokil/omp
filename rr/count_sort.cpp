#include <time.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <limits>
#include <chrono>

const int MAX_ELEM_VALUE = std::numeric_limits<int>::max();

void count_sort_parallel(int a[], int n) {
	int *temp = new int[n];
	#pragma omp parallel for shared(a, n, temp)
	for (int i = 0; i < n; i++) {
		int count = 0;
		for (int j = 0; j < n; j++) {
			if (a[j] < a[i]) {
				count++;
			} else if (a[j] == a[i] && j < i) {
				count++;
			}
		}
		temp[count] = a[i];
	}

	#pragma omp parallel for shared(a, n, temp)
	for (int i = 0; i < n; i++) {
		a[i] = temp[i];
	}
}

int* generate_array(int size) {
	srand(time(0));
	auto arr = new int[size];
	for (int i = 0; i < size; ++i) {
		arr[i] = rand() % MAX_ELEM_VALUE;
	}
	return arr;
}

template<typename Func>
long test(Func f) {
	auto start = std::chrono::high_resolution_clock::now();
	f();
	auto finish = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count();
	return duration;
	std::cout << "Time: " << duration << " ns\n";
}

long sort_with_time_bench(int * array, int length, int parallelism) {
	omp_set_num_threads(parallelism);
	auto f = [&array, &length, &parallelism]() { 
		count_sort_parallel(array, length); 
	};
	return test(f);
}

int main(int argc, char* argv[]) {
	int elems, parallelism = 1;
	if (argc == 3) {
		elems = atoi(argv[1]);
		parallelism = atoi(argv[2]);
	} else {
		std::cout << "Usage: " << argv[0] << " <elements> [<threads>]\n";
		return 1;
	}
	auto max_threads = omp_get_max_threads();
	if (parallelism > max_threads) {
		std::cout << "Max threads: " << max_threads << "\n";
		return 1;
	}

	auto nums = generate_array(elems);
	auto time_ns = sort_with_time_bench(nums, elems, parallelism);
	std::cout << "Elements:\t" << elems       <<   " ";
	std::cout << "Threads:\t"  << parallelism <<   " ";
	std::cout << "Time: "     << time_ns     << " ns";
	std::cout << std::endl;
	
	return 0;
}