#include <time.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <limits>
#include <chrono>

const int MAX_ELEM_VALUE = std::numeric_limits<int>::max();

void swap_elems(int * array, int i, int j) {
	auto temp = array[i];
	array[i] = array[j];
	array[j] = temp;
}

void insertion_sort(int* arr, int start, int end) {
	for (auto i = start; i < end; i++) {
		auto j = i;
		while (j > start && arr[j] < arr[j-1]) {
			swap_elems(arr, j, j-1);
			j--;
		}
	}
}

void insertion_sort_parallel(int* arr, int length, int parallelism) {
	auto size = length / parallelism;
	#pragma omp parallel for
	for (int i = 1; i <= parallelism; ++i) {
		auto current_end = size * i;
		auto current_start = size * (i - 1);
		if (length - current_end < size) {
			current_end = length;
		}
		insertion_sort(arr, current_start, current_end);
	}
	insertion_sort(arr, 0, length);
}

int* generate_array(int size) {
	srand(time(0));
	auto arr = new int[size];
	for (int i = 0; i < size; ++i) {
		arr[i] = rand() % MAX_ELEM_VALUE;
	}
	return arr;
}

template<typename Func>
long test(Func f) {
	auto start = std::chrono::high_resolution_clock::now();
	f();
	auto finish = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count();
	return duration;
	std::cout << "Time: " << duration << " ns\n";
}

long sort_with_time_bench(int * array, int length, int parallelism) {
	if (parallelism > 1) {
		omp_set_num_threads(parallelism);
		auto f = [&array, &length, &parallelism]() { 
			insertion_sort_parallel(array, length, parallelism); 
		};
		return test(f);
	}
	auto f = [&array, &length]() { 
		insertion_sort(array, 0, length); 
	};
	return test(f);
}

int main(int argc, char* argv[]) {
	int elems, parallelism = 1;
	if (argc == 2) {
		elems = atoi(argv[1]);
	} else if (argc == 3) {
		elems = atoi(argv[1]);
		parallelism = atoi(argv[2]);
	} else {
		std::cout << "Usage: " << argv[0] << " <elements> [<threads>]\n";
		return 1;
	}

	auto max_threads = omp_get_max_threads();
	if (parallelism > max_threads) {
		std::cout << "Max threads: " << max_threads << "\n";
		return 1;
	}

	auto nums = generate_array(elems);
	auto time_ns = sort_with_time_bench(nums, elems, parallelism);
	std::cout << "Elements:\t" << elems       <<   " ";
	std::cout << "Threads:\t"  << parallelism <<   " ";
	std::cout << "Time: "     << time_ns     << " ns";
	std::cout << std::endl;
	
	return 0;
}