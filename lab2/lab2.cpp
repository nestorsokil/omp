#include <omp.h>
#include <iostream>
#include <stdio.h>
#include <time.h>

const int SIZE = 5000; // 5k x 5k matrix

long** get_matrix() {
	srand(time(0));
	long** matrix = new long*[SIZE];
	for (int i = 0; i < SIZE; ++i){
		matrix[i] = new long[SIZE];
		for (int j = 0; j < SIZE; ++j) {
			matrix[i][j] = rand() % 1000 + 1; // random num in range [0, 1000]
		}
	}
	return matrix;
}

int main() {
	long** matrix = get_matrix();
	int vsize = SIZE;
    int hsize = SIZE;

	long* results = new long[vsize];
	omp_set_num_threads(3);

	#pragma omp parallel for shared(matrix, hsize, results) 
	for (int i = 0; i < vsize; i++) {
		long* row = matrix[i];
		long sum = 0;
		for (int j = 0; j < hsize; j++) {
			long elem = row[j];
			if (elem % 2 == 0) {
				sum += elem;
			}
		}
		results[i] = sum;
	}

	long sumrows = 0;
	for (int i = 0; i < vsize; ++i) {
		sumrows += results[i];
	}

	std::cout << std::endl << "Sum of evens: " << sumrows << std::endl;

	return 0;
}