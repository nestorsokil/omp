#include <omp.h>
#include <iostream>
#include <math.h>

const int N = 19;

double f(double x) {
	return sin(x) / ((x*x) + 1);
}

int main() {
	omp_set_num_threads(3);

	double result = 1.0;
	#pragma omp parallel for reduction(*:result)
	for (int x = 1; x < N; x++) {
		result = result * f(x);
	}

	printf(" N = %d\n", N);
	printf("___\n");
	printf("| | sin x / (x^2 + 1) = %e\n", result);
	printf("| |\n");
	printf("x=1\n");

	return 0;
}