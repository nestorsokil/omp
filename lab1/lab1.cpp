#include <omp.h>
#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
	omp_lock_t lock;
	omp_init_lock(&lock);
	omp_set_num_threads(4);
	double pow = 1;
	int shifts[] = { 30, 30, 30, 29 }; // sum components of 119
	#pragma omp parallel for shared (pow, lock, shifts)
	for (int i = 0; i < 4; i++) {
		int shift = shifts[i];
		int shifted = (1 << shift);
		omp_set_lock(&lock);
		pow *= shifted;
		omp_unset_lock(&lock);
	}

	cout << "2 pow 119 = " << pow << endl;
	return 0;
}