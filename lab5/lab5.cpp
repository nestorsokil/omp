#include <time.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <algorithm>
#include <map>

struct IndexAndValue {
	int index;
	std::string value;
	IndexAndValue(int index, std::string value) {
		this->index = index;
		this->value = value;
	} 
};

const int SIZE = 100;
std::map<int, std::string> NAMES;
int** EDGES;
int* VISITED;

// initialize graph edges and nodes
void init() {
	srand(time(0));
	EDGES = new int*[SIZE];
	VISITED = new int[SIZE];
	#pragma opm parallel for
	for (int i = 0; i < SIZE; i++) {
		EDGES[i] = new int[SIZE];
		NAMES[i] = std::to_string(i);
		for (int j = 0; j < SIZE; j++) {
			EDGES[i][j] = (i != j) ? (rand() % 2) : 0;
		}
	}
}

// breadth-first search
std::list<int> bfs(int startWith, std::string toFind) {
	std::list<IndexAndValue*> queue;
	std::list<int> visited;
	visited.push_back(startWith);
	auto start = new IndexAndValue(startWith, NAMES[startWith]);
	queue.push_back(start);
	while(queue.size() != 0) {
		auto node = queue.back(); 
		queue.pop_back();
		auto neighbours = EDGES[node->index];
		#pragma opm parallel for
		for (int i = 0; i < SIZE; i++) {
			if (neighbours[i] != 0 && (std::find(visited.begin(), visited.end(), i) == visited.end())) {
				auto next = new IndexAndValue(i, NAMES[i]);
				if (next->value.compare(toFind) == 0) {
					return visited;
				}
				visited.push_back(next->index);
				queue.push_back(next);
			}
		}
	}
	throw "NOT FOUND";
}

// output the path to a output-stream
void print_path(std::list<int> path, std::ostream& output) {
	for (auto it : path) {
			output << " -> " << it;
	}
	output << std::endl;
}

int main() {
	try {
		omp_set_num_threads(3);
		init();	
		// search for "19" starting from 0
		auto path = bfs(0, "19");
		print_path(path, std::cout);
    } catch (char const* error) {
    	std::cout << "Program failed with reason: " << error << std::endl;
    	return 1;
    }
	return 0;
}